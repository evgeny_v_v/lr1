#include <stdio.h>                                                                                                                                                                             
#include <stdlib.h>                                                                                                                                                                            
#include <sys/types.h>                                                                                                                                                                         
#include <sys/ipc.h>                                                                                                                                                                           
#include <sys/msg.h>                                                                                                                                                                           
#include <string.h>                                                                                                                                                                            
#include "msgtypes.h"                                                                                                                                                                          
                                                                                                                                                                                               
int main (int argc, char * argv [])                                                                                                                                                            
                                                                                                                                                                                               
{                                                                                                                                                                                              
        struct msg_t message;                                                                                                                                                                  
        int msgid;                                                                                                                                                                             
        char * response = "OK!!!";                                                                                                                                                             
        msgid = msgget (KEY, 0666 | IPC_CREAT); // ������� ������� ���������                                                                                                                   
        msgrcv (msgid, &message, sizeof (message), 2, 0); // ���� ���������                                                                                                                    
        printf ("Client (pid = %i) sent: %s", message.snd_pid, message.body);                                                                                                                  
        message.mtype = 1;                                                                                                                                                                     
        message.snd_pid = getpid ();                                                                                                                                                           
        strcpy(message.body, response);                                                                                                                                                        
        msgsnd (msgid, &message, sizeof (message), 0); // �������� �����                                                                                                                       
        msgrcv (msgid, &message, sizeof (message), 2, 0); // ���� �������������                                                                                                                
        msgctl (msgid, IPC_RMID, 0); // ������� �������                                                                                                                                        
        return EXIT_SUCCESS;                                                                                                                                                                   
}                                                                                                                                                                                              
   